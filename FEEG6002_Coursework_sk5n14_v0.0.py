# -*- coding: utf-8 -*-
"""
University of Southampton
FEEG6002 Coursework 2015/2016: PDE methods
===============================================================================

by
Stefan Koch
ID: 28189221

November 22th, 2015

Version 0.0
last edit: -
===============================================================================
"""
# Librarys and loadings
import numpy as np
import scipy as sp
import scipy.linalg
import matplotlib
import matplotlib.pyplot as plt

""" Task 1: Function definitions """

# Function Definition
def get_k(i, j, n):
    """
    Convert from (i,j) indices in 2D to k index in U column
    vector.
    """
    return i * n + j 
    
    
def fill(A, ilst=None, jlst=None,directions='UDLR'):
    """
    Fill the stencil coefficients in matrix A corresponding to
    nodes (i,j), where i is taken from 'ilst' and j from 'jlst'
    """
    # all non-boundary nodes
    if ilst is None:
        # all rows except first and last one
        ilst = range(1, n - 1)
    if jlst is None:
        # all columns except first and last one
        jlst = range(1, n - 1) #
    # Loop over all nodes (i,j) in [ilst x jlst] and fill A
    for i in ilst:
        for j in jlst:
            k = get_k(i, j, n)
            A[k, k] = -4      #center
            if 'U' in directions:
                A[k, k-n] = 1 #up
            if 'D' in directions:
                A[k, k+n] = 1 #down
            if 'L' in directions:
                A[k, k-1] = 1 #left
            if 'R' in directions:
                A[k, k+1] = 1 #right
                
def get_A(n):
    """Return 2D Laplace matrix A using solution 2"""
    # Initialize the matrix
    A = np.zeros((n**2, n**2))

    fill(A)
    # Fill top, bottom, left and right boundaries:
    fill(A, ilst=[0], directions='DLR') # top
    fill(A, ilst=[n-1], directions='ULR') # bottom
    fill(A, jlst=[0], directions='UDR') # left
    fill(A, jlst=[n-1], directions='UDL') # right
    # Fill corners
    fill(A, [0], [0], 'RD') # top left
    fill(A, [0], [n-1], 'LD') # top right
    fill(A, [n-1], [0], 'UR') # bottom left
    fill(A, [n-1], [n-1], 'LU') # bottom right
    return A


# define the solution vecotr of the linear system A*U=b               
def get_b(n, rho=2):
    """Return column vector of size n^2 containing the boundary conditions."""
    b = np.zeros(n**2)
    if (n % 2 == 0): #even 
        b[(0.5*n-1)*(n+1)] = rho
    else: #odd
        b[(0.5*n**2)] = rho
    return b

    
def embed(T, u0):
    """Embed the array T giving the temperature at the inner nodes in
    the domain into a larger array including the boundary temperatures
    """
    ufull = np.zeros((N,N))
    ufull[0] = u0
    ufull[1:-1, 1:-1] = T
    return ufull
    
def plot_pcolor(ufull):
    """Plot temperature in the domain using pcolor"""
    N = ufull.shape[0]
    x = y = np.linspace(0, 1, N)
    X, Y = np.meshgrid(x,y)
    plt.pcolor(X, Y, ufull)
    plt.axis('scaled')
    plt.colorbar()
    plt.xlabel('x (m)')
    plt.ylabel('y (m)')
    plt.title('T(x,y) on %dx%d grid' % (N,N))
    
    
""" Task 2: Function definitions """
def gaussSeidel(A,b,N=50,x=None,tol=1e-10):
    """Solves the equation Ax=b via the Gauss-Seidel method with
       Successive-over-relaxation (SOR)."""
       
    #create initial value for relaxation-factor omega
    omega = 1
    #create values for adaptive relaxion parameter (default)
    p = 1
    k = 10
    # Create an initial guess if needed
    if x is None:
        x = np.zeros(len(A[0]))
    
    D = np.diag(A)
    R = A-np.diagflat(D)
    #prepare empty arrays to store additional output values
    errlst = []
    iterlst = []
    Wlst=[]
    
    xold=x.copy()
    for i in range(N):
        for j in range(len(x)):
            #GS-SOR for each line in the linear system
            x[j]=omega*(b[j]-np.dot(R[j],x))/D[j]+(1-omega)*x[j]
        #pprint(x)
        
        #Compute adaptive relaxation factor ofter step k+p
        deltaX = np.sqrt(np.dot(x-xold,x-xold))
        if i==k*(i/k):
            dx1 = deltaX
        if i==((k*(i/k))+p):
            dx2 = deltaX
            omega = 2.0/(1.0+np.sqrt(1.0-(dx2/dx1)**(1.0/p)))
        Wlst.append(omega)
        deltaX_old = deltaX.copy()
        # calculate Residuals 
        if i == 0:
            xold=np.linalg.norm(x)
        res=np.linalg.norm(xold-x)/np.linalg.norm(x)
        #pprint(res)
        errlst.append(res)
        #save result for next iteration
        xold=x.copy()
        
        
        iterlst.append(i+1)
        if res<tol:
            return x #,errlst, iterlst, Wlst
            
            
            
    
#============================================================================              
"""Task 1"""
#Domain size
N=21
#reduced domain without boundary nodes
n=N-2;
#Boundary Condition for u
u0=0;
#Build A and b and Solve problem
A=get_A(n)
b=get_b(n)
U = sp.linalg.solve(A, b)
u1 = U.reshape((n, n))
u1full = embed(u1, u0)
#plot_pcolor(ufull)

"""Task2"""

U2 = gaussSeidel(A,b,N=1000,x=None,tol=1e-11)
u2 = U2.reshape((n, n))
u2full = embed(u2, u0)

"""Single-Output-Check"""
print"\n *** Results ***"
#Center Point @ (0.5,0.5)
cp=0.5*N;
cen_Val1=(u1full[cp-1,cp]+u1full[cp+1,cp]-4*u1full[cp,cp]+u1full[cp,cp-1]+u1full[cp,cp+1])
print "\n-Task1: Value of PDE(1) at (0.5/0.5)=",cen_Val1

cp=0.5*N;
cen_Val2=u2full[cp-1,cp]+u2full[cp+1,cp]-4*u2full[cp,cp]+u2full[cp,cp-1]+u2full[cp,cp+1]
print "\n-Task2: Value of PDE(1) at (0.5/0.5)=",cen_Val2










