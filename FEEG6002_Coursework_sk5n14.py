# -*- coding: utf-8 -*-
"""
University of Southampton
FEEG6002 Coursework 2015/2016: PDE methods
===============================================================================

by
Stefan Koch
ID: 28189221

January 6th, 2016

Version 3.1
last edit: 5.01.2016
===============================================================================
"""
# Library import
import numpy as np
import scipy as sp
import scipy.linalg
import sys

# Definition of Functions

"""Functions for Task1"""

def build_A1(n):
    """
    Create matrix A of the linear system of equations using a 2nd order
    Central-Differencing-Scheme and a block-structure approach for a given 
    number of unknowns "n" in each direction
    """       
    # Define main-diagonal block-matrix
    Adiag = -4 * np.eye(n)
    Aupper = np.diag([1] * (n - 1), 1)
    Alower = np.diag([1] * (n - 1), -1)
    A = Adiag + Aupper + Alower
    # Create diagonal Matrix consisting of n main-diagonal blocks
    alst = [A] * n
    A1 = sp.linalg.block_diag(*alst)
    # Define upper-diagonal block-matrix
    Dupper = np.diag(np.ones(n * (n - 1)), n)
    # Define lower-diagonal block-matrix
    Dlower = np.diag(np.ones(n * (n - 1)), -n)
    # Combine main-,upper- and lower-diagonal block Matrix
    A1 += Dupper + Dlower
    return A1
             
def create_b(n, rho):
    """
    Create column vector b of size n^2 containing values of function rho.
    """
    # Set rho to zero everywhere
    b = np.zeros(n**2)
    # Set rho at center of vector b to satisfy rho(0.5,0.5)=2
    b[(0.5*n**2)] = rho
    return b
    
"""Functions for Task2"""

def residuals(i,x,xold):
    """Calculate residual/error between two iterations"""
    res=np.linalg.norm(xold-x)/np.linalg.norm(x)
    return res
    
def ispositive(A):
    """Return True if the input matrix is positive-definite"""
    return np.all(np.linalg.eigvals(A)>0)
    
def issymmetric(A):
    """Return True if the input matrix is symmetric"""
    return np.all(A.T==A)

def gaussSeidel_SOR_ARF(A,b,N=1000,x=None,omega=None,tol=1e-12,ARF='off'):
    """
    Solves the linear system Ax=b using the Gauss-Seidel method with
    Successive-Over-Relaxation (SOR) and adaptive relaxation factor (ARF)
    """
    # Check if the matrix A satisfies the convergence critera for the solver
    if (issymmetric(A) or ispositive(A)):
        # Create an initial guess of x if not provided
        if x is None:
            x = np.zeros(len(A[0]))
        # Default values for adaptive relaxion parameter
        p = 1
        k = 10
        # Initial Guess for relaxation parameter omega
        if omega is None:
            omega=1.0
        # Split matrix A in a diagonal and off-diagonal matrices
        D = np.diag(A)
        R = A-np.diagflat(D)
        # Initialising of xold for adaptive relaxation and residual calculation
        xold=x.copy()
        # Gauss-Seidel iterations (Main-loop)
        for i in range(N):
            for j in range(len(x)):
                #GaussSeidel-aSOR for each line in the linear system
                x[j]=omega*(b[j]-np.dot(R[j],x))/D[j]+(1-omega)*x[j]
            
            # Compute new relaxation factor the first time ofter step k+p
            # Omega is updated every (k+p)th iteration step
            if ARF=='on':
                deltaX = np.sqrt(np.dot(x-xold,x-xold))
                if i==k*(i/k):
                    dx1 = deltaX
                if i==((k*(i/k))+p):
                    dx2 = deltaX
                    # Calculate new optimised relaxation factor omega
                    omega = 2.0/(1.0+np.sqrt(1.0-(dx2/dx1)**(1.0/p)))
    
            # calculate Residuals/error 
            res=residuals(i,x,xold)
            # Store current x
            xold=x.copy()
            # Generate warning to standard-error stream if solution did not converge
            if (i==(N-1)):
                print >> sys.stderr, '*** Warning: Gauss-Seidel solver exceeded maximum number of iterations ***'
            # Stop iterationg if error tolerance is achieved or after N iterations
            if (res<tol):
                return x
        # Warning if convergence critera are not satisfied by the matrix A
        else:
            print >> sys.stderr, '*** Warning: Matrix A does not satisfy convergence critera! Solve process not started ***'
    return x
            
"""Functions for Task 3""" 
    
def build_A3(n):
    """
    Create matrix A of the linear system of equations using a 4nd order
    Central-Differencing-Scheme and a block-structure approach for a given 
    number of unknowns "n" in each direction
    """
    # Define main-diagonal block-matrix     
    Adiag = -60 * np.eye(n)
    Aupper1 = np.diag([16] * (n - 1), 1)
    Aupper2 = np.diag([-1] * (n - 2), 2)
    Alower1 = np.diag([16] * (n - 1), -1)
    Alower2 = np.diag([-1] * (n - 2), -2)
    A = Adiag + Aupper1 + Alower1 + Aupper2 + Alower2
    # Create diagonal Matrix consisting of n x main-diagonal blocks
    alst = [A] * n
    A = sp.linalg.block_diag(*alst)
    # Define upper-diagonal block-matrix
    Dupper = np.diag(16*np.ones(n * (n - 1)), n)
    Dupper2 = np.diag(-1*np.ones(n * (n - 2)), 2*n)
    # Define lower-diagonal block-matrix
    Dlower = np.diag(16*np.ones(n * (n - 1)), -n)
    Dlower2 = np.diag(-1*np.ones(n * (n - 2)), -2*n)    
    # Combine main-,upper- and lower-diagonal block Matrix
    A += Dupper + Dlower + Dupper2 + Dlower2
    return A
    
"""Functions for Task 4""" 

def build_redBlackA(A):
    """ Reorders Matrix A1 (Task 1) according to red-black ordering rule"""
    l=len(A) 
    # Initialise empty arrays for later use       
    dr=[]
    db=[]
    c=[]
    # Create upper-left matrix block "Dr" corresponding to red nodes 
    for i in range(0,l,2):
        for j in range(0,l,2):
            dr.append(A[i,j])
    Dr=np.asarray(dr)
    Dr=Dr.reshape(((l+1)/2),((l+1)/2))
    # Create lower-right matrix block "Db" corresponding to black nodes
    for i in range(1,l,2):
        for j in range(1,l,2):
            db.append(A[i,j])
    Db=np.asarray(db)
    Db=Db.reshape(((l-1)/2),((l-1)/2))   
    # Create lower-left matrix block "C" corresponding to red nodes
    for i in range(1,l,2):
        for j in range(0,l,2):
            c.append(A[i,j])
    C=np.asarray(c)
    C=C.reshape(((l-1)/2),((l+1)/2))
    return Dr,Db,C
    
def build_redBlack_b(b):
    """Seperates the values in b according to the coloring (red/black)"""
    # Initialise empty arrays for later use
    br=[]
    bb=[]
    for i in range(len(b)):
        # Even indices are related to red nodes
        if (i % 2 == 0):
            br.append(b[i])
        # Odd indices are related to black nodes   
        else:
            bb.append(b[i])
    return br,bb
    
def redBlackGaussSeidel_SOR_ARF(Dr,Db,C,br,bb,N,xb,omega,tol=1e-11,ARF='off'):
    """
    Solves a "red-black" reordered linear system of equations using the
    "red-black" formulation of the Gauss-Seidel method with
    Successive-Over-Relaxation (SOR) and adaptive relaxation parameter (arp)
    """
    # Create an initial guess of x if not provided
    if xb is None:
        xb = np.ones(((n**2-1)/2))
    # Initial Guess for relaxation parameter omega if not provided
    if omega is None:
        omega=1.0
    # Default values for adaptive relaxation parameter omega
    p = 2
    k =10
    
    Drinv=np.linalg.inv(Dr)
    Dbinv=np.linalg.inv(Db)
    # Initialising of xold for adaptive relaxation and residual calculation
    xold=xb.copy()
    # Gauss-Seidel iterations (Main-loop)
    for i in range(N):
        # Compute updated values (n+1) for "red" based on old "black" values n
        if (i==0):
            # First update of red nodes without relaxation (no xr available)
            xr = (np.dot(Drinv,(br-np.dot(C.T,xb))))
        else:
            xr = omega*(np.dot(Drinv,(br-np.dot(C.T,xb))))+(1-omega)*xr
        # Update "black" values according to the updated "red" values
        xb = omega*(np.dot(Dbinv,(bb-np.dot(C,xr))))+(1-omega)*xb
        
        # Compute new relaxation factor the first time ofter step k+p
        # Omega is updated every (k+p)th iteration step
        if (ARF=='on'):
            deltaX = np.sqrt(np.dot(xb-xold,xb-xold))
            if i==k*(i/k):
                dx1 = deltaX
            if i==((k*(i/k))+p):
                dx2 = deltaX
                omega = 2.0/(1.0+np.sqrt(1.0-(dx2/dx1)**(1.0/p)))
        
        # Calculate Residuals/error of xb ("black" nodes)
        res=residuals(i,xb,xold)
        # Store current xb
        xold=xb.copy();
        # Generate warning to standard-error stream if solution did not converge
        if (i==(N-1)):
            print >> sys.stderr, '*** Warning: Red-black solver exceeded maximum number of iterations ***'
        # Stop iterationg if error tolerance is achieved or after N iterations
        if (res<tol):
            return xr,xb
    return xr,xb
    
def build_A4(Dr,DB,C):
    """Build matrix A4 based on block-matrices Dr,Db and C"""
    A=np.concatenate((Dr,C.T),axis=1)
    B=np.concatenate((C,Db),axis=1)
    A=np.concatenate((A,B),axis=0)
    return A4
    
"""General functions""" 
            
def embed(U, u0):
    """
    Embed the matrix U containing the values u at the inner nodes in a larger
    matrix including the boundary conditions u0
    """
    # Initialise ufull
    ufull = np.zeros((N,N))
    # Embed internal node values u
    ufull[1:-1, 1:-1] = U
    return ufull                       
    
"""===========================Problem setup================================="""            

# Domain size
N=11
# Inner domain without boundary nodes
n=N-2
# Calucate general grid spacing (assuming equally spaced nodes in x and y)
h=1/float(N-1)
# Boundary Condition
u0=0;

"""Task 1"""
A1=build_A1(n)
# Set rho to satisfy boundary condition (2nd Order Central-Difference)
rho1 = 2*h**2
b1=create_b(n,rho1)
# Solve linear system A1x=b1 using built-in solver
U1 = sp.linalg.solve(A1, b1)
u1 = U1.reshape((n, n))
u1full = embed(u1, u0)

"""Task2"""
# Using matrix A1 and column vecor b from task 1, but solve linear system
# with self-coded gaussSeidel_SOR_arp solver
U2 = gaussSeidel_SOR_ARF(A1,b1,N=5000,x=None,omega=1.8,tol=1e-12,ARF='off')
u2 = U2.reshape((n, n))
u2full = embed(u2, u0)

"""Task3"""
A3=build_A3(n)
# Set rho to satisfy boundary condition (4th Order Central-Differece)
rho3=2*12*h**2
b3=create_b(n,rho3)
# Solve linear system A3x=b3 using built-in solver
U3 = sp.linalg.solve(A3, b3)
u3 = U3.reshape((n, n))
u3full = embed(u3, u0)

"""Task4"""
Dr,Db,C=build_redBlackA(A1)
br,bb=build_redBlack_b(b1)
# Solve reordered linear system using self-coded red-black gauss Seidel method with SOR and arp
xr,xb=redBlackGaussSeidel_SOR_ARF(Dr,Db,C,br,bb,5000,xb=None,omega=1.8,tol=1e-12,ARF='off')
# Rearrange colour-separated results xr (red) and xb (black) to a normal ordered vector
xrb=np.ones((n**2))
xrb[::2]=xr
xrb[1::2]=xb
u4 = xrb.reshape((n, n))
u4full = embed(u4, u0)

"""Single-Output-Check"""

print"\n *** Results ***"
# Center point (0.5,0.5)
cp=int(0.5*N);

cen_Val1=(u1full[cp-1,cp]+u1full[cp+1,cp]-4*u1full[cp,cp]+u1full[cp,cp-1]+u1full[cp,cp+1])/h**2
print "\n-Task1: Value of PDE(1) at (0.5/0.5)=",cen_Val1
cen_Val2=(u2full[cp-1,cp]+u2full[cp+1,cp]-4*u2full[cp,cp]+u2full[cp,cp-1]+u2full[cp,cp+1])/h**2
print "\n-Task2: Value of PDE(1) at (0.5/0.5)=",cen_Val2
cen_Val3=((-1*u3full[cp-2,cp])+(-1*u3full[cp+2,cp])+16*u3full[cp-1,cp]+16*u3full[cp+1,cp]-60*u3full[cp,cp]+16*u3full[cp,cp-1]+16*u3full[cp,cp+1]+(-1*u3full[cp,cp+2])+(-1*u3full[cp,cp-2]))/(12*h**2)
print "\n-Task3: Value of PDE(1) at (0.5/0.5)=",cen_Val3
cen_Val4=(u4full[cp-1,cp]+u4full[cp+1,cp]-4*u4full[cp,cp]+u4full[cp,cp-1]+u4full[cp,cp+1])/h**2
print "\n-Task4: Value of PDE(1) at (0.5/0.5)=",cen_Val4










